module InVADER_trigger_top ( in, stdby_in, stdby1, osc_clk, led0, led1, led2, led3, led4, led5, led6, led7, ltrig, qswitch, ctrig, cgate );

input	stdby_in ;
input   [3:0] in ;
output	stdby1, osc_clk ;
output	led0, led1, led2, led3, led4, led5, led6, led7, ltrig, qswitch, ctrig, cgate ;

wire	stby_flag ;
reg		[22:0] cnt ;
reg     ltrig;    	 //laser trig
reg 	qswitch; 	 //qswitch trig
reg     ctrig;     	 //cam trig
reg     cgate;     	 //gate trig
wire    com_ltrig; 	 //output laser trig
wire 	com_qswitch; //output qswitch trig
wire    com_ctrig; 	 //output cam trig
wire    com_cgate; 	 //output cam gate


//assign clkout = clki ;
//assign rstout = rstn ;

// Internal Oscillator
   defparam OSCH_inst.NOM_FREQ = "88.67";		//  88.67  --> Freq for 11.278 ns clk period

OSCH OSCH_inst( .STDBY(stdby1 ), 		// 0=Enabled, 1=Disabled also Disabled with Bandgap=OFF
                .OSC(osc_clk),
                .SEDSTDBY());		//  this signal is not required if not using SED - see TN1199 for more details.

pwr_cntrllr pcm1 (.USERSTDBY(stdby_in ), .CLRFLAG(stby_flag ), .CFGSTDBY(1'b0 ),
    .STDBY(stdby1 ), .SFLAG(stby_flag ) );

//  LED BLinking test FPGA for LATTICE X02-1200
//  Turn on every other LED @ 1/2 second

always @(posedge osc_clk)
begin
  ltrig   <= com_ltrig;
  qswitch <= com_qswitch;
  ctrig   <= com_ctrig;
  cgate   <= com_cgate;
end

always @(posedge osc_clk or posedge stdby_in)
if (stdby_in || ~led2)
		cnt <= 0;
	else
	    cnt <= cnt + 1;

assign led0 = stdby_in ? 1'b1 : stdby_in;
assign led1 = stdby_in ? 1'b1 : ~stdby_in;
// assign led2 = stdby_in ? 1'b1 : ~(cnt > 2982000);							// 4473000 - 20.00 Hz
// assign led3 = stdby_in ? 1'b1 : ~(cnt > 2982000);							// 2982000 - 30.00 Hz
assign led2 = stdby_in ? 1'b1 : ~(cnt > 4473000);							// 8946000 - 10.00 Hz, 44730000 - 2.00 Hz
assign led3 = stdby_in ? 1'b1 : ~(cnt > 4473000);							// 8866000 - 100ms, 29820000 - 3.00 Hz
assign led4 = stdby_in ? 1'b1 : ~in[0];
assign led5 = stdby_in ? 1'b1 : ~in[1];
assign led6 = stdby_in ? 1'b1 : ~in[2];
assign led7 = stdby_in ? 1'b1 : ~in[3];

// Channel A, T0
assign com_ltrig = stdby_in ? 1'b1 :  cnt < 21458;                              // 240,000 ns

////// TIMING CALCULATIONS EXAMPLE ///////
// make the pulse with 1 microSecond (1000 ns) -> 1000 / 11.278 = 88.7,  so add 89 counts to the start count to get the stop count
// delay of 745 ns, 745/11.278 = 66
// delay of 1245 ns, 1245/11.278 = 110
// make pulse 5 uS, 5000/11.278 = 443
// make pulse of 500 ns, 500/11.278 = 44
// make pulse of 56 ns 56/11.278 = 5
// make pulse of 80 ns - 8

////////Timing Modes////////
// in = 1 is RAMAN
// in = 2 Mineral phosphorence
// All other modes are not

// Channel B, T0 + 180us delay
assign com_qswitch =  stdby_in ? 1'b1 : (in > 0 && cnt > (15960) && cnt < (15960+8870)); // 15960 == 180us QS delay


// Channel C, Delay of B - 50ns
assign com_ctrig = stdby_in ? 1'b1 : (in == 1 && cnt > (15955) && cnt < (15955+4435)) |  // mode "1" 15955 == Delay of B (100us) - 50ns // 4435 == pulse width 50us
                                     (in == 2 && cnt > (15955) && cnt < (15955+4435)) |  // mode "2"
									 (in == 3 && cnt > (15955) && cnt < (15955+4435)) |
									 (in == 4 && cnt > (15955) && cnt < (15955+4435)) |
									 (in == 5 && cnt > (15955) && cnt < (15955+4435)) |
									 (in == 6 && cnt > (15955) && cnt < (15955+4435)) |
									 (in == 7 && cnt > (15955) && cnt < (15955+4435)) |
									 (in == 8 && cnt > (15955) && cnt < (15955+4435));

// Channel D, Delay of C but pulse width of 50ns
assign com_cgate = stdby_in ? 1'b1 : (in == 1 && cnt > (15955) && cnt < (15955+6)) |     // Mode "1" 6 == 68ns pulse width
                                     (in == 2 && cnt > (15955) && cnt < (15955+500)) |   // Mode "2" 500 == 5.6us pulse width
									 (in == 3 && cnt > (15955) && cnt < (15955+6)) |
									 (in == 4 && cnt > (15955) && cnt < (15955+6)) |
									 (in == 5 && cnt > (15955) && cnt < (15955+6)) |
									 (in == 6 && cnt > (15955) && cnt < (15955+6)) |
									 (in == 7 && cnt > (15955) && cnt < (15955+6)) |
									 (in == 8 && cnt > (15955) && cnt < (15955+6));
endmodule
