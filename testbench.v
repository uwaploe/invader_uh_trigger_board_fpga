`timescale 1ns/1ns

module testbench;
	
	
reg		[22:0] cnt;
reg     ltrig;    	 //laser trig
reg 	qswitch; 	 //qswitch trig
reg     ctrig;     	 //cam trig
reg     cgate;     	 //gate trig

wire    com_ltrig; 	 //output laser trig
wire 	com_qswitch; //output qswitch trig
wire    com_ctrig; 	 //output cam trig
wire    com_cgate; 	 //output cam gate
	
	Default_w_standby_top dut(.ltrig(ltrig), .qswitch(qswitch), .ctrig(ctrig));
	
	initial
	begin
		